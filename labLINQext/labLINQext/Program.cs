﻿using System;
using System.Linq;
namespace labLINQext
{
    class Program
    {
        static void Main(string[] args)
        {
            var arr = new string[] {"Москва", "Сочи", "Орел", "Самара", "Тула", "Смоленск", "Ялта", "Сумы"};
            //Console.WriteLine(string.Join(' ', arr));
            Console.WriteLine(string.Join(Environment.NewLine, arr));
            Console.WriteLine("....");
            var x1 = string.Join(' ', arr.Where(v => v.StartsWith('С') && v.Length > 4).ToArray());
            Console.WriteLine(x1);
            Console.WriteLine("....");
            var x2 = arr.OrderBy(v => v).Select(v => $"[{v}] - {v.Contains('а')}").ToArray();
            Console.WriteLine(string.Join(Environment.NewLine, x2));
        }
    }
}
