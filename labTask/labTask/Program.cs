﻿using System;
using System.Threading;
using System.Threading.Tasks;

namespace labTask
{
    class Program
    {
        private static void MyTask1()
        {
            Console.WriteLine("Task 1");
        }
        static void Main(string[] args)
        {
            Console.WriteLine("Start");

            //(1)
            var t1 = new Task(MyTask1);
            t1.Start();

            //(2)
            new Task(() => Console.WriteLine("Task 2"));
            //(3)
            Task.Run(() => Console.WriteLine("Task 3"));
            //(4)
            var t4 = new Task(MyTask4);
            t4.Start();
            //(5)
            new Task(async () => { await Task.Delay(200); Console.WriteLine("task 5"); }).Start();
            Console.WriteLine("End");
            Console.ReadKey();
        }

        private static void MyTask4()
        {
            Console.WriteLine("Task4 - begin");
            Thread.Sleep(1000);
            Console.WriteLine("Task4 - end");
        }
    }
}
