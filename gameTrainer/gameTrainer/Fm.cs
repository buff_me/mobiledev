﻿using System;
using System.Drawing;
using System.Windows.Forms;

namespace gameTrainer
{
    public partial class Fm : Form
    {
        private Game game;
        public Fm()
        {
            InitializeComponent();

            game = new Game();
            game.Change += G_Change;
            game.DoReset();

            buYes.Click += (s, e) => game.DoAnswer(true);
            buNo.Click += (s, e) => game.DoAnswer(false);

            buReset.Click += (s, e) => game.DoReset();

            buYes.MouseEnter += ButtonsAnswer_MouseEnter;
            buYes.MouseLeave += ButtonsAnswer_MouseLeave;

            buNo.MouseEnter += ButtonsAnswer_MouseEnter;
            buNo.MouseLeave += ButtonsAnswer_MouseLeave;
        }

        private void ButtonsAnswer_MouseLeave(object sender, EventArgs e)
        {
            if (sender is Control btn)
            {
                btn.ForeColor = Color.FromArgb(255, 255, 255); ;
            }
        }

        private void ButtonsAnswer_MouseEnter(object sender, EventArgs e)
        {
            if (sender is Control btnYes && sender == buYes)
            {
                btnYes.ForeColor = Color.FromArgb(0, 192, 0);
            }
            else if(sender is Control btnNo)
            {
                btnNo.ForeColor = Color.Red;
            }
        }

        private void G_Change(object sender, EventArgs e)
        {
            laText.Text = game.CodeText;
            laCorrect.Text = $"Верно = {game.CountCorrect}";
            laWrong.Text = $"Неверно = {game.CountWrong}";
        }
    }
}
