﻿using System;

public class Game
{
    public int CountCorrect { get; private set; }
    public int CountWrong { get; private set; }


    private int FirstArg;
    private int SecondArg;


    private int CorrectSum;
    private decimal CorrectDiv;
    private int CorrectDiff;
    private int CorrectMult;


    public string CodeText { get; private set; }
    private bool AnswerCorrect;


    public event EventHandler Change;


    public void DoReset()
    {
        CountCorrect = 0;
        CountWrong = 0;
        DoContinue();
    }
    private int Generic(int first, int second)
    {
        Random rnd = new Random();
        return rnd.Next(first, second);
    }
    private string GetSum()
    {
        FirstArg = Generic(1, 100);
        SecondArg = Generic(1, 100);
        CorrectSum = FirstArg + SecondArg;
        if (Generic(0,2) == 1)
        {
            AnswerCorrect = true;
            return $"{FirstArg} + {SecondArg} = {CorrectSum}";
        }
        else
        {
            AnswerCorrect = false;
            return $"{FirstArg} + {SecondArg} = {Generic(CorrectSum+1, CorrectSum+10)}";
        }
    }
    private string GetMult()
    {
        FirstArg = Generic(1, 100);
        SecondArg = Generic(1, 100);
        CorrectMult = FirstArg * SecondArg;
        if (Generic(0, 2) == 1)
        {
            AnswerCorrect = true;
            return $"{FirstArg} * {SecondArg} = {CorrectMult}";
        }
        else
        {
            AnswerCorrect = false;
            return $"{FirstArg} * {SecondArg} = {Generic(CorrectMult+1, CorrectMult+10)}";
        }
    }
    private string GetDiff()
    {
        FirstArg = Generic(1, 100);
        SecondArg = Generic(1, 100);
        CorrectDiff = FirstArg - SecondArg;
        if (Generic(0, 2) == 1)
        {
            AnswerCorrect = true;
            return $"{FirstArg} - {SecondArg} = {CorrectDiff}";
        }
        else
        {
            AnswerCorrect = false;
            return $"{FirstArg} - {SecondArg} = {Generic(CorrectDiff-10, CorrectDiff-1)}";
        }
    }
    private string GetDivision()
    {
        FirstArg = Generic(1, 100);
        SecondArg = Generic(1, 100);
        CorrectDiv = Decimal.Round(Convert.ToDecimal(FirstArg) / Convert.ToDecimal(SecondArg), 2);
        if (Generic(0, 2) == 1)
        {
            AnswerCorrect = true;
            return $"{FirstArg} / {SecondArg} = {CorrectDiv}";
        }
        else
        {
            AnswerCorrect = false;
            return $"{FirstArg} / {SecondArg} = {CorrectDiv + Convert.ToDecimal(Generic(0,3))}";
        }
    }
    private void DoContinue()
    {
       switch(Generic(0, 4))
        {
            case 0:
               CodeText = GetSum();
                break;
            case 1:
               CodeText = GetDiff();
                break;
            case 2:
               CodeText = GetMult();
                break;
            case 3:
               CodeText = GetDivision();
                break;
        }
        Change?.Invoke(this, EventArgs.Empty);
    }
    public void DoAnswer(bool answer)
    {
        if (answer == AnswerCorrect)
            CountCorrect++;
        else
            CountWrong++;
        DoContinue();
    }
}
