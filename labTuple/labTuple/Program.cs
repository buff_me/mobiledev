﻿using System;
using System.ComponentModel.DataAnnotations;

namespace labTuple
{
    class Program
    {
        static void Main(string[] args)
        {
            var x0 = 5;
            int x00 = 5;
            //(1)
            var x1 = (2, 4);
            (int, int) x11 = (2, 4);
            Console.WriteLine(x1.Item1);
            Console.WriteLine(x1.Item2);
            Console.WriteLine();
            //(2) название полей кортежа
            (int min, int max) x2 = (2, 4);
            Console.WriteLine(x2.min);
            Console.WriteLine(x2.max);
            Console.WriteLine();
            //(3) название полей кортежа через инициализацию
            var x3 = (min: 2, max: 4);
            Console.WriteLine(x3.min);
            Console.WriteLine(x3.max);
            Console.WriteLine();
            //(4) отдельные переменные для каждого поля кортежа
            var (min, max) = (2, 4);
            Console.WriteLine(min);
            Console.WriteLine(max);
            Console.WriteLine();
            //(5) получение кортежа
            var x5 = GetX5();
            Console.WriteLine(x5.Item1);
            Console.WriteLine(x5.Item2);
            Console.WriteLine();
            //(6) кортеж с именами
            var x6 = GetX6();
            Console.WriteLine(x6.min);
            Console.WriteLine(x6.max);
            Console.WriteLine();
            //(7) передача кортежа в качетсве параметра
            var x7 = GetX7((4, 5), 7);
            Console.WriteLine(x7.Item1);
            Console.WriteLine(x7.Item2);
            Console.WriteLine();
        }

        private static (int, int) GetX5() => (2, 4);
        private static (int min, int max) GetX6() => (3, 6);
        private static (int, int) GetX7((int, int) p, int v) => (p.Item1 + p.Item2, v);
    }
}
