﻿namespace labkey
{
    partial class fm
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.laText = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // laText
            // 
            this.laText.AutoSize = true;
            this.laText.Location = new System.Drawing.Point(306, 165);
            this.laText.Name = "laText";
            this.laText.Size = new System.Drawing.Size(50, 20);
            this.laText.TabIndex = 0;
            this.laText.Text = "label1";
            // 
            // fm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(827, 561);
            this.Controls.Add(this.laText);
            this.Name = "fm";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label laText;
    }
}

