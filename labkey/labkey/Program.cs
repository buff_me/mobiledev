using System;
using System.Windows.Forms;
using app = System.Windows.Forms.Application;
namespace labkey
{
     class Program
    {
        /// <summary>
        ///  The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            app.SetHighDpiMode(HighDpiMode.SystemAware);
            app.EnableVisualStyles();
            app.SetCompatibleTextRenderingDefault(false);
            app.Run(new fm());
        }
    }
}
