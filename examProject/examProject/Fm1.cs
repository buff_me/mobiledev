﻿using System;
using System.Drawing;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace examProject
{
    public partial class Fm1 : Form
    {
        Timer formTimer = new Timer();
        Timer beforeStart = new Timer();
        Game game = new Game();
        private int beforeTicks = 3;
        public Fm1()
        {
            InitializeComponent();
            ClearButtons();
            FillButtons();
            beforeStart.Start();
            beforeStart.Interval = 1000;
            beforeStart.Tick += BeforeStart_Tick;
            exitBtn.Click += (object s, EventArgs e) => Close();
        }

        private void BeforeStart_Tick(object sender, EventArgs e)
        {
           timerLbl.Text = $"До начала игры: 00:0{beforeTicks}";
            if (beforeTicks == 0)
            {
                HideTextButton();
                beforeStart.Stop();
                formTimer.Start();
                formTimer.Interval = 1000;
                formTimer.Tick += FormTimer_Tick;
            }
            beforeTicks--;

        }

        private void FormTimer_Tick(object sender, EventArgs e)
        {
            timerLbl.Text = game.text;
            if(game.text == "Оставшееся время 00:00")
            {
                formTimer.Stop();
            }
        }

        private void ClearButtons()
        {
            foreach (var button in Controls.OfType<Button>())
            {
                if (button.Name != "exitBtn")
                    button.Text = "";
            }
        }
        private void HideTextButton()
        {
            foreach (var button in Controls.OfType<Button>())
            {
                if (button.Name != "exitBtn")
                    button.ForeColor = Color.FromArgb(192, 192, 255);
            }
        }
        private void FillButtons()
        {
            int i = 0;
            foreach (var button in Controls.OfType<Button>())
            {
                if (button.Name != "exitBtn")
                {
                    button.Text = game.digits[i++].ToString();
                    button.Click += Button_Click;
                }
            }
        }

        private void Button_Click(object sender, EventArgs e)
        {
            var button = sender as Button;
            countLbl.Text = $"Счёт: {game.setCount(sender).ToString()}";
            if (game.raiseCount)
            {
                button.ForeColor = Color.FromArgb(0, 0, 0);
            }
        }
    }
}
