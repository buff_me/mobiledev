﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;

namespace examProject
{
    class Game
    {
        public List<int> digits = new List<int>();
        private Timer timer = new Timer();
        private int ticks = 25;
        public string text = "";
        public int index = 0;
        private int count = 0;
        public bool raiseCount = false;
        public Game()
        {
            TimerSetup();
            FillList();
            Shuffle();

        }
        private void FillList()
        {
            for(int i = 0; i< 10; i++)
            {
                digits.Add(i);
            }
        }
        private void Shuffle()
        {
            Random rand = new Random();

            for (int i = digits.Count - 1; i >= 1; i--)
            {
                int j = rand.Next(i + 1);
                int tmp = digits[j];
                digits[j] = digits[i];
                digits[i] = tmp;
            }
        }
        private void Timer_Tick(object sender, EventArgs e)
        {
            ticks--;
            text = ticks < 10 ? $"Оставшееся время: 00:0{ticks}" : $"Оставшееся время: 00:{ticks}";
            if (ticks == 0)
            {
                timer.Stop();
            }
        }
        private void TimerSetup()
        {

            timer.Interval = 1000;
            timer.Start();
            timer.Tick += Timer_Tick;
        }
        public int setCount(object sender)
        {
            int pressButtonNumber = Convert.ToInt32(sender.ToString().Split(',')[1].Split(':')[1]);
            if (pressButtonNumber == index)
            {
                index++;
                raiseCount = true;
                return count += 5;
            }
            raiseCount = false;
            return count;
        }
    }
}
