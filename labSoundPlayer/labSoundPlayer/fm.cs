﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Media;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace labSoundPlayer
{
    public partial class fm : Form
    {
        private SoundPlayer soundPlayer = new SoundPlayer();
        public fm()
        {
            InitializeComponent();
            soundPlayer.Stream = Properties.Resources.kick;
            buPlay.Click += (s, e) => soundPlayer.Play();
            buStop.Click += (s, e) => soundPlayer.Stop();
            buPlayCycle.Click += (s, e) => soundPlayer.PlayLooping();
            buBeep.Click += (s, e) => SystemSounds.Beep.Play();
        }
    }
}
