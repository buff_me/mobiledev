﻿namespace labSoundPlayer
{
    partial class fm
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.buPlay = new System.Windows.Forms.Button();
            this.buStop = new System.Windows.Forms.Button();
            this.buPlayCycle = new System.Windows.Forms.Button();
            this.buBeep = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // buPlay
            // 
            this.buPlay.Location = new System.Drawing.Point(28, 114);
            this.buPlay.Name = "buPlay";
            this.buPlay.Size = new System.Drawing.Size(94, 29);
            this.buPlay.TabIndex = 0;
            this.buPlay.Text = "Play";
            this.buPlay.UseVisualStyleBackColor = true;
            // 
            // buStop
            // 
            this.buStop.Location = new System.Drawing.Point(28, 149);
            this.buStop.Name = "buStop";
            this.buStop.Size = new System.Drawing.Size(94, 29);
            this.buStop.TabIndex = 1;
            this.buStop.Text = "Stop";
            this.buStop.UseVisualStyleBackColor = true;
            // 
            // buPlayCycle
            // 
            this.buPlayCycle.Location = new System.Drawing.Point(28, 184);
            this.buPlayCycle.Name = "buPlayCycle";
            this.buPlayCycle.Size = new System.Drawing.Size(155, 29);
            this.buPlayCycle.TabIndex = 2;
            this.buPlayCycle.Text = "Play looping";
            this.buPlayCycle.UseVisualStyleBackColor = true;
            // 
            // buBeep
            // 
            this.buBeep.Location = new System.Drawing.Point(28, 231);
            this.buBeep.Name = "buBeep";
            this.buBeep.Size = new System.Drawing.Size(230, 29);
            this.buBeep.TabIndex = 3;
            this.buBeep.Text = "System sound \"Beep\"";
            this.buBeep.UseVisualStyleBackColor = true;
            // 
            // fm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.buBeep);
            this.Controls.Add(this.buPlayCycle);
            this.Controls.Add(this.buStop);
            this.Controls.Add(this.buPlay);
            this.Name = "fm";
            this.Text = "Form1";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button buPlay;
        private System.Windows.Forms.Button buStop;
        private System.Windows.Forms.Button buPlayCycle;
        private System.Windows.Forms.Button buBeep;
    }
}

