﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace labPaint
{
    public partial class Fm : Form
    {
        private readonly Bitmap b;
        private readonly Graphics g;
        private Point startPoint;
        private Pen myPen;

        public Fm()
        {
            InitializeComponent();

            b = new Bitmap(Screen.PrimaryScreen.Bounds.Width, Screen.PrimaryScreen.Bounds.Height);
            g = Graphics.FromImage(b);
            g.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.AntiAlias;

            myPen = new Pen(pxColor1.BackColor, 10);
            myPen.StartCap = myPen.EndCap = System.Drawing.Drawing2D.LineCap.Round;

            pxImage.MouseDown += (s, e) => startPoint = e.Location;
            pxImage.MouseMove += PxImage_MouseMove;
            pxImage.Paint += (s, e) => e.Graphics.DrawImage(b, 0, 0);

            trPenWidth.Value = Convert.ToInt32(myPen.Width);
            trPenWidth.ValueChanged += (s, e) => myPen.Width = trPenWidth.Value;

            // перенес в PxColorAll_MouseDown
            //pxColor1.Click += (s, e) => myPen.Color = pxColor1.BackColor;
            //pxColor2.Click += (s, e) => myPen.Color = pxColor2.BackColor;
            //pxColor3.Click += (s, e) => myPen.Color = pxColor3.BackColor;
            //pxColor4.Click += (s, e) => myPen.Color = pxColor4.BackColor;

            pxColor1.MouseDown += PxColorAll_MouseDown;
            pxColor2.MouseDown += PxColorAll_MouseDown;
            pxColor3.MouseDown += PxColorAll_MouseDown;
            pxColor4.MouseDown += PxColorAll_MouseDown;

            buImageClear.Click += delegate
            {
                g.Clear(SystemColors.Control);
                pxImage.Invalidate();
            };

            buSaveToFile.Click += BuSaveToFile_Click;
            buLoadFromFile.Click += BuLoadFromFile_Click;
            buAddRandomStars.Click += BuAddRandomStars_Click;


            //HW
            // функционал похожий на обычный Windows Paint
            // рисование и  левой и правой кнопкой мыши
            // 
        }

        private void BuAddRandomStars_Click(object sender, EventArgs e)
        {
            var rnd = new Random();
            for (int i = 0; i < 300; i++)
            {
                g.FillEllipse(new SolidBrush(Color.FromArgb(rnd.Next(256), rnd.Next(256), rnd.Next(256))),
                    rnd.Next(b.Width),
                    rnd.Next(b.Height),
                    rnd.Next(1,10),
                    rnd.Next(1,10)
                    );
            }
            pxImage.Invalidate();
        }

        private void BuLoadFromFile_Click(object sender, EventArgs e)
        {
            OpenFileDialog dialog = new OpenFileDialog();
            dialog.Filter = "Image Files(*.BMP;*.JPG;*.GIF)|*.BMP;*.JPG;*.GIF|All files (*.*)|*.*";
                if (dialog.ShowDialog() == DialogResult.OK)
            {
                g.Clear(SystemColors.Control);
                g.DrawImage(Bitmap.FromFile(dialog.FileName), 0, 0);
                pxImage.Invalidate();
            }
        }

        private void BuSaveToFile_Click(object sender, EventArgs e)
        {
            SaveFileDialog dialog = new SaveFileDialog();
            dialog.Filter = "JPG Files(*.JPEG)|*.JPG";
            if (dialog.ShowDialog() ==DialogResult.OK) 
            {
                b.Save(dialog.FileName);
            }
        }

        private void PxColorAll_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                if (sender is PictureBox x)
                {
                    myPen.Color = x.BackColor;
                }
            }

            if (e.Button == MouseButtons.Right)
            {
            if (sender is PictureBox x)
                {
                ColorDialog colorDialog = new ColorDialog();
                    colorDialog.Color = x.BackColor;
                    if (colorDialog.ShowDialog() == DialogResult.OK)
                    {
                        x.BackColor = colorDialog.Color;
                        myPen.Color = x.BackColor;
                    }
                }
            }
        }

        private void PxImage_MouseMove(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                g.DrawLine(myPen, startPoint, e.Location);
                startPoint = e.Location;
                pxImage.Invalidate();
            }
        }
    }
}
