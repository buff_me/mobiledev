﻿namespace labPaint
{
    partial class Fm
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.buImageClear = new System.Windows.Forms.Button();
            this.trPenWidth = new System.Windows.Forms.TrackBar();
            this.pxColor4 = new System.Windows.Forms.PictureBox();
            this.pxColor3 = new System.Windows.Forms.PictureBox();
            this.pxColor2 = new System.Windows.Forms.PictureBox();
            this.pxColor1 = new System.Windows.Forms.PictureBox();
            this.pxImage = new System.Windows.Forms.PictureBox();
            this.buSaveToFile = new System.Windows.Forms.Button();
            this.buLoadFromFile = new System.Windows.Forms.Button();
            this.buAddRandomStars = new System.Windows.Forms.Button();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.trPenWidth)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pxColor4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pxColor3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pxColor2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pxColor1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pxImage)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.buAddRandomStars);
            this.panel1.Controls.Add(this.buLoadFromFile);
            this.panel1.Controls.Add(this.buSaveToFile);
            this.panel1.Controls.Add(this.buImageClear);
            this.panel1.Controls.Add(this.trPenWidth);
            this.panel1.Controls.Add(this.pxColor4);
            this.panel1.Controls.Add(this.pxColor3);
            this.panel1.Controls.Add(this.pxColor2);
            this.panel1.Controls.Add(this.pxColor1);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(200, 450);
            this.panel1.TabIndex = 0;
            // 
            // buImageClear
            // 
            this.buImageClear.Location = new System.Drawing.Point(12, 124);
            this.buImageClear.Name = "buImageClear";
            this.buImageClear.Size = new System.Drawing.Size(158, 25);
            this.buImageClear.TabIndex = 2;
            this.buImageClear.Text = "Clear";
            this.buImageClear.UseVisualStyleBackColor = true;
            // 
            // trPenWidth
            // 
            this.trPenWidth.Location = new System.Drawing.Point(29, 73);
            this.trPenWidth.Minimum = 1;
            this.trPenWidth.Name = "trPenWidth";
            this.trPenWidth.Size = new System.Drawing.Size(104, 45);
            this.trPenWidth.TabIndex = 1;
            this.trPenWidth.Value = 1;
            // 
            // pxColor4
            // 
            this.pxColor4.BackColor = System.Drawing.Color.MediumOrchid;
            this.pxColor4.Location = new System.Drawing.Point(135, 12);
            this.pxColor4.Name = "pxColor4";
            this.pxColor4.Size = new System.Drawing.Size(35, 45);
            this.pxColor4.TabIndex = 0;
            this.pxColor4.TabStop = false;
            // 
            // pxColor3
            // 
            this.pxColor3.BackColor = System.Drawing.Color.Yellow;
            this.pxColor3.Location = new System.Drawing.Point(94, 12);
            this.pxColor3.Name = "pxColor3";
            this.pxColor3.Size = new System.Drawing.Size(35, 45);
            this.pxColor3.TabIndex = 0;
            this.pxColor3.TabStop = false;
            // 
            // pxColor2
            // 
            this.pxColor2.BackColor = System.Drawing.Color.Blue;
            this.pxColor2.Location = new System.Drawing.Point(53, 12);
            this.pxColor2.Name = "pxColor2";
            this.pxColor2.Size = new System.Drawing.Size(35, 45);
            this.pxColor2.TabIndex = 0;
            this.pxColor2.TabStop = false;
            // 
            // pxColor1
            // 
            this.pxColor1.BackColor = System.Drawing.Color.Red;
            this.pxColor1.Location = new System.Drawing.Point(12, 12);
            this.pxColor1.Name = "pxColor1";
            this.pxColor1.Size = new System.Drawing.Size(35, 45);
            this.pxColor1.TabIndex = 0;
            this.pxColor1.TabStop = false;
            // 
            // pxImage
            // 
            this.pxImage.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pxImage.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pxImage.Location = new System.Drawing.Point(200, 0);
            this.pxImage.Name = "pxImage";
            this.pxImage.Size = new System.Drawing.Size(600, 450);
            this.pxImage.TabIndex = 1;
            this.pxImage.TabStop = false;
            // 
            // button1
            // 
            this.buSaveToFile.Location = new System.Drawing.Point(12, 177);
            this.buSaveToFile.Name = "button1";
            this.buSaveToFile.Size = new System.Drawing.Size(158, 23);
            this.buSaveToFile.TabIndex = 3;
            this.buSaveToFile.Text = "Save to file";
            this.buSaveToFile.UseVisualStyleBackColor = true;
            // 
            // button2
            // 
            this.buLoadFromFile.Location = new System.Drawing.Point(12, 224);
            this.buLoadFromFile.Name = "button2";
            this.buLoadFromFile.Size = new System.Drawing.Size(158, 23);
            this.buLoadFromFile.TabIndex = 4;
            this.buLoadFromFile.Text = "Load from file";
            this.buLoadFromFile.UseVisualStyleBackColor = true;
            // 
            // button3
            // 
            this.buAddRandomStars.Location = new System.Drawing.Point(10, 272);
            this.buAddRandomStars.Name = "button3";
            this.buAddRandomStars.Size = new System.Drawing.Size(160, 23);
            this.buAddRandomStars.TabIndex = 5;
            this.buAddRandomStars.Text = "Add random Stars";
            this.buAddRandomStars.UseVisualStyleBackColor = true;
            // 
            // Fm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.pxImage);
            this.Controls.Add(this.panel1);
            this.Name = "Fm";
            this.Text = "labPaint";
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.trPenWidth)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pxColor4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pxColor3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pxColor2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pxColor1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pxImage)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.TrackBar trPenWidth;
        private System.Windows.Forms.PictureBox pxColor4;
        private System.Windows.Forms.PictureBox pxColor3;
        private System.Windows.Forms.PictureBox pxColor2;
        private System.Windows.Forms.PictureBox pxColor1;
        private System.Windows.Forms.PictureBox pxImage;
        private System.Windows.Forms.Button buImageClear;
        private System.Windows.Forms.Button buAddRandomStars;
        private System.Windows.Forms.Button buLoadFromFile;
        private System.Windows.Forms.Button buSaveToFile;
    }
}

