﻿using System;
using System.IO;

namespace labFile
{
    class Program
    {
        static void Main(string[] args)
        {
            string path = Path.Combine(Directory.GetCurrentDirectory(), "temp123.txt");
            //string path = Path.Combine(Path.GetTempPath(), "temp123.txt");
            var strings = new string[] { "Орел", "Тула", "Курск", "Москва", "Самара", "Сочи", "Томск", "Иркутск" };
            File.WriteAllText(path, string.Join(Environment.NewLine, strings));
            var v2 = File.ReadAllText(path);
            Console.WriteLine(v2);
            Console.WriteLine("---");
            Console.WriteLine(File.Exists(path));
            File.Delete(path);
            FileInfo fileInfo = new FileInfo(path);
            Console.WriteLine(fileInfo.FullName);
            Console.WriteLine(fileInfo.Length);
            Console.WriteLine(fileInfo.Name);
        }
    }
}
