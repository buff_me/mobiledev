﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;

namespace snake
{
    public partial class Fm1 : Form
    {
        int cols = 50, rows = 25, score = 0, dx = 0, dy = 0, front = 0, back = 0;
        Bit[] Snake = new Bit[1250];
        List<int> available = new List<int>();
        bool[,] visit;
        Random rand = new Random();

        Timer timer = new Timer();
        public Fm1()
        {
            InitializeComponent();
            Initial();
            launchTimer();
            KeyPreview = true;
            restartBtn.Enabled = false;
            exitBtn.Click += (object s, EventArgs e) => Close();
            restartBtn.Click += (object s, EventArgs e) =>
              {
                  restartPan.Hide();
                  exitBtn.Hide();

                  Reset();
                  Initial();
                  launchTimer();
              };
            restartPan.Hide();
            exitBtn.Hide();
        }
        private void Reset()
        {
            foreach (var item in Snake)
            {
                Controls.Remove(item);
            }
            restartBtn.Enabled = false;
            available.Clear();
            Array.Clear(visit, 0, visit.Length);
            rand = new Random();
            timer = new Timer();
            score = 0;
            scoreLabel.Text = $"Score: {score}";
            dx = 0;
            dy = 0;
            front = 0;
            back = 0;
            Array.Clear(Snake, 0, Snake.Length);
        }
        private void launchTimer()
        {
            timer.Interval = 100;
            timer.Tick += move;
            timer.Start();
        }

        private void Fm1_KeyDown(object sender, KeyEventArgs e)
        {
            dx = dy = 0;
            switch (e.KeyCode)
            {
                case Keys.Right:
                    dx = 20;
                    break;
                case Keys.Left:
                    dx = -20;
                    break;
                case Keys.Up:
                    dy = -20;
                    break;
                case Keys.Down:
                    dy = 20;
                    break;
                case Keys.W:
                    dy = -20;
                    break;
                case Keys.D:
                    dx = 20;
                    break;
                case Keys.A:
                    dx = -20;
                    break;
                case Keys.S:
                    dy = 20;
                    break;
            }
        }

        private void move(object sender, EventArgs e)
        {
            int x = Snake[front].Location.X, y = Snake[front].Location.Y;
            if (dx == 0 && dy == 0) return;
            else if (gameOver(x + dx, y + dy))
            {
                timer.Stop();
                //MessageBox.Show("Game over");
                restartPan.Show();
                exitBtn.Show();

                restartBtn.Enabled = true;
                return;
            }
            else if (collisionFood(x + dx, y + dy))
            {
                score += 1;
                scoreLabel.Text = "Score: " + score.ToString();
                if (hits((y + dy) / 20, (x + dx) / 20)) return;
                Bit head = new Bit(x + dx, y + dy);
                front = (front - 1 + 1250) % 1250;
                Snake[front] = head;
                visit[head.Location.Y / 20, head.Location.X / 20] = true;

                Controls.Add(head);
                --timer.Interval;
                randomFood();
            }
            else
            {
                if (hits((y + dy) / 20, (x + dx) / 20))
                {
                    return;
                }
                visit[Snake[back].Location.Y / 20, Snake[back].Location.X / 20] = false;

                front = (front - 1 + 1250) % 1250;
                Snake[front] = Snake[back];
                Snake[front].Location = new Point(x + dx, y + dy);
                back = (back - 1 + 1250) % 1250;
                visit[(y + dy) / 20, (x + dx) / 20] = true;
            }
        }

        private void randomFood()
        {
            available.Clear();
            for (int i = 0; i < rows; i++)
            {
                for (int j = 0; j < cols; j++)
                {
                    if (!visit[i, j])
                    {
                        available.Add(i * cols + j);
                    }
                }
            }

            int index = rand.Next(available.Count) % available.Count;
            foodLabel.Left = (available[index] * 20) % (Width);
            foodLabel.Top = (available[index] * 20) / (Width) * 20;
        }

        private bool hits(int x, int y)
        {
            if(visit[x, y])
            {
                timer.Stop();
                //MessageBox.Show("Snake eats its body");
                restartPan.Show();
                exitBtn.Show();

                restartBtn.Enabled = true;
                return true;
            }
            return false;
        }

        private bool collisionFood(int x, int y)
        {
            return x == foodLabel.Location.X && y == foodLabel.Location.Y;
        }

        private bool gameOver(int x, int y)
        {
            return x < 0 || y < 0 || x > 980 || y > 480;
        }
        private void Initial()
        {
            visit = new bool[rows, cols];
            Bit head = new Bit((rand.Next() % cols) * 20, (rand.Next() % rows) * 20);
            foodLabel.Location = new Point((rand.Next() % cols) * 20, (rand.Next() % rows) * 20);

            for(int i = 0; i < rows; ++i)
            {
                for (int j = 0; j < cols; j++)
                {
                    visit[i, j] = false;
                    available.Add(i * cols + j);
                }
            }
            visit[head.Location.Y / 20, head.Location.X / 20] = true;

            available.Remove(head.Location.Y / 20 * cols + head.Location.X / 20);
            Controls.Add(head);
            Snake[front] = head;
        }
    }
}
