﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace snake
{
    class Bit : Label
    {
        public Bit(int x, int y)
        {
            Location = new Point(x, y);
            Size = new Size(20, 20);
            BackColor = Color.Orange;
            Enabled = false;
        }
    }
}
