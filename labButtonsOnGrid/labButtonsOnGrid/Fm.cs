﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace labButtonsOnGrid
{
    public partial class Fm : Form
    {
        private Button[,] bu;
        public int Rows { get; private set; } = 3;
        public int Cols { get; private set; } = 4;
        public Fm()
        {
            InitializeComponent();
            CreateButtons();
            ResizeButtons();
            Resize += (s, e) => ResizeButtons();

        }

        private void ResizeButtons()
        {
            int cellWidth = ClientSize.Width / Cols;
            int cellHeight = ClientSize.Height / Rows;
            for (int i = 0; i < Rows; i++)
            {
                for (int j = 0; j < Cols; j++)
                {
                    bu[i, j].Width = cellWidth;
                    bu[i, j].Height = cellHeight;
                    bu[i, j].Location = new Point(cellWidth * j, cellHeight * i);
                }
            }
        }

        private void CreateButtons()
        {
            int n = 1;
            bu = new Button[Rows, Cols];
            for (int i = 0; i < Rows; i++)
            {
                for (int j = 0; j < Cols; j++)
                {
                    bu[i, j] = new Button();
                    bu[i, j].Text = n++.ToString();
                    bu[i, j].Font = new Font("Segoe UI", 20);
                    bu[i, j].Click += ButtonAll_Click;
                    Controls.Add(bu[i, j]);
                }
            }
        }

        private void ButtonAll_Click(object sender, EventArgs e)
        {
            if(sender is Control x)
            {
                MessageBox.Show(x.Text);
            }
        }
    }
}
